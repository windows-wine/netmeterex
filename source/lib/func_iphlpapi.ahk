; � Drugwash 2016 v1.0
; Wrapper for iphlpapi.dll
; #include updates.ahk (in lib), call updates()
; #include Net.ahk (in lib)
; #include func_DebugFunctions.ahk
; #include func_FileMap.ahk
; #include func_FormatMessage.ahk
; #include func_GetCSIDL.ahk
; #include func_GetFileVersionInfo.ahk
; #include func_ws2_32.ahk

;================================================================
iphlpapi(pn="", por="")
;================================================================
{
#IncludeAgain __GALS.ahk
}
;================================================================
GetAdapterIndex(name, ByRef idx)	; compatibility 2000/XP/Vista/? Requires WStr as name input
;================================================================
{
Static pr
Global PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
r := DllCall(pr, "Str", name, PtrP, idx)
;idx &= 0xFFFF		; return only the low-word since we don't know how to interpret the high-word
if r
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nidx=" idx "`nname=" name, r)
return r
}
;================================================================
GetAdaptersInfo(ByRef buf)			; IP_ADAPTER_INFO
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, &buf, PtrP, sz:=0))=111	; ERROR_BUFFER_OVERFLOW
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
GetBestInterface(ip)
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if r := DllCall(pr, "UInt", inet_addr(ip), PtrP, idx)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nidx=" idx, r)
;else return (idx & 0xFFFF)
else return idx
}
;================================================================
GetBestRoute(ByRef buf, dest, src="0")	; MIB_IPFORWARDROW (can take CPU to 100% !!!)
;================================================================
{
Static pr
Global Ptr
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if dest is not integer
	dest := inet_addr(dest)		; ws2_32 convert string to IP (UInt)
if src is not integer
	src := inet_addr(src)		; ws2_32 convert string to IP (UInt)
VarSetCapacity(buf, 56)			; MANDATORY, won't work with uninitialized buffer !!!
if r := DllCall(pr, "UInt", dest, "UInt", src, Ptr, &buf)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`ndest=" dest "`nsrc=" src, r)
return r
}
;================================================================
GetFriendlyIfIndex(idx)				; Should return backward-compatible index (lower 24 bits - 16?)
;================================================================
{
Static pr
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if !r := DllCall(pr, "UInt", idx)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nidx=" idx, r)
return r
}
;================================================================
GetIfEntry(ByRef buf, idx)			; MIB_IFROW
;================================================================
{
Global Ptr
Static pr
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
VarSetCapacity(buf, 860, 0)
NumPut(idx, buf, 512, "UInt")
if r := DllCall(pr, Ptr, &buf)
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nidx=" idx "`nbuf=" &buf, r)
return r
}
;================================================================
GetIfTable(ByRef buf, ord=0)		; MIB_IFTABLE -> MIB_IFROW
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, &buf, PtrP, sz:=0, "UInt", ord))=122	; ERROR_INSUFFICIENT_BUFFER
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz, "UInt", ord)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
GetInterfaceInfo(ByRef buf)			; IP_INTERFACE_INFO -> IP_ADAPTER_INDEX_MAP
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, 0, PtrP, sz:=0))=122	; ERROR_INSUFFICIENT_BUFFER
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
GetIpAddrTable(ByRef buf, ord=0)	; MIB_ADDRTABLE -> MIB_IPADDRROW
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, &buf, PtrP, sz:=0, "UInt", ord))=122	; ERROR_INSUFFICIENT_BUFFER
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz, "UInt", ord)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
GetIpForwardTable(ByRef buf, ord=0)	; MIB_IPFORWARDTABLE -> MIB_IPFORWARDROW
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, &buf, PtrP, sz:=0, "UInt", ord))=122	; ERROR_INSUFFICIENT_BUFFER
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz, "UInt", ord)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
GetIpNetTable(ByRef buf, ord=0)		; MIB_IPNETTABLE -> MIB_IPNETROW
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, &buf, PtrP, sz:=0, "UInt", ord))=122	; ERROR_INSUFFICIENT_BUFFER
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz, "UInt", ord)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
GetNetworkParams(ByRef buf)		; FIXED_INFO
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, &buf, PtrP, sz:=0))=111	; ERROR_BUFFER_OVERFLOW
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
GetTcpTable(ByRef buf, ord=0)	; MIB_TCPTABLE -> MIB_TCPROW
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, &buf, PtrP, sz:=0, "UInt", ord))=122	; ERROR_INSUFFICIENT_BUFFER
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz, "UInt", ord)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
GetUdpTable(ByRef buf, ord=0)	; MIB_UDPTABLE -> MIB_UDPROW
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if (r := DllCall(pr, Ptr, &buf, PtrP, sz:=0, "UInt", ord))=122	; ERROR_INSUFFICIENT_BUFFER
	{
	VarSetCapacity(buf, sz, 0)
	if !DllCall(pr, Ptr, &buf, PtrP, sz, "UInt", ord)
		return sz
	}
OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nsz=" sz, r)
}
;================================================================
IpReleaseAddress(p)			; DHCP only
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if r := DllCall(pr, Ptr, p)			; pointer to IP_ADAPTER_INDEX_MAP
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`np=" p, r)
else return TRUE
}
;================================================================
IpRenewAddress(p)			; DHCP only
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if r := DllCall(pr, Ptr, p)			; pointer to IP_ADAPTER_INDEX_MAP
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`np=" p, r)
else return TRUE
}
;================================================================
IsLocalAddress(addr)
;================================================================
{
Static pr
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if addr is not integer
addr := inet_addr(addr)
if r := DllCall(pr, "UInt", addr)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`naddr=" addr, r)
return r
}
;================================================================
NotifyAddrChange(h, ByRef o)	; compatibility 2000/XP/Vista/?
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if h
	{
	VarSetCapacity(o, 20, 0)	; OVERLAPPED structure must be declared outside the function !
	NumPut(h, o, 12, Ptr)		; hEvent
	r := DllCall(pr, PtrP, h, Ptr, &o)	; allows asynchronous notification
	}
else r := DllCall(pr, Ptr, 0, Ptr, 0)	; blocked call
if (r && (r1 := WSAGetLastError()) != 997)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nhEvent=" h, r)
		. "`n" FormatMessage("WSAGetLastError()", r1)
else return TRUE
}
;================================================================
GetIpStatistics(ByRef s)			; MIB_IPSTATS (only for IPv4)
;================================================================
{
Static pr
Global Ptr
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
VarSetCapacity(s, 92, 0)
if r := DllCall(pr, Ptr, &s)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nMIB_IPSTATS sz=" VarSetCapacity(s), r)
else return TRUE
}
;================================================================
GetRTTAndHopCount(addr, ByRef hops, ByRef rtt, maxH="-1")
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if addr is not integer
	addr := inet_addr(addr)
if !r := DllCall(pr, "UInt", addr, PtrP, hops, "UInt", maxH, PtrP, rtt)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`naddr=" addr, A_LastError)
else return TRUE
}
;================================================================
GetUniDirectionalAdapterInfo(ByRef buf)	; IP_UNIDIRECTIONAL_ADAPTER_ADDRESS -> IPAddr
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
VarSetCapacity(buf, 4, 0)
if r := DllCall(pr, PtrP, buf, PtrP, sz)
	{
	if sz <= 4							; 0 in XP, 4 in 98SE, when no such adapter present
		return 0
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nReq. sz=" nsz, r)
	VarSetCapacity(buf, sz, 0)
	if r := DllCall(pr, PtrP, buf, PtrP, sz)
		OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nBuffer sz=" sz, r)
	else return sz
	}
else return sz
}
;================================================================
;							UNTESTED ! ! !
;================================================================
NotifyRouteChange(h, ByRef o)	; compatibility 2000/XP/Vista/?
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if h
	{
	VarSetCapacity(o, 20, 0)	; OVERLAPPED structure must be outside the function !
	NumPut(h, o, 12, Ptr)		; hEvent
	r := DllCall(pr, PtrP, h, Ptr, &o)	; allows asynchronous notification
	}
else r := DllCall(pr, Ptr, 0, Ptr, 0)	; blocked call
if (r && (r1 := WSAGetLastError()) != 997)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nhEvent=" h, r)
		. "`n" FormatMessage("WSAGetLastError()", r1)
else return TRUE
}
;================================================================
SetIpTTL(v)
;================================================================
{
Static pr
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if r := DllCall(pr, "UInt", v)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nvalue=" v, r)
else return TRUE
}
;================================================================
SendARP(dest, src=0, sep="-")	; compatibility 2000/XP/Vista/?
;================================================================
{
Static pr
Global Ptr, PtrP
if !pr
	pr:=iphlpapi(A_ThisFunc)	; retrieve procedure address from loader function
if dest is not integer
	dest := inet_addr(dest)
if src is not integer
	src := inet_addr(src)
VarSetCapacity(buf, sz:=8, 0)
if r := DllCall(pr, "UInt", dest, "UInt", src, Ptr, &buf, PtrP, sz)
	OutputDebug, % ErrorLevel := FormatMessage( A_ThisFunc "()`nPointer=" pr "`nBuffer sz=" sz, r)
else return Net_MakeMAC(&buf, sz, sep)
}
;================================================================
