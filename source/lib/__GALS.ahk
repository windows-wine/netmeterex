; Generic API List & Store v1.1
; � Drugwash 2016
; This script is meant to be included in every library wrapper,
; offering an automated listing of exported APIs and their procedure addresses.
; WARNING: Every wrapper that uses this will be affected by subsequent alterations of this script !!!
; Requires updates.ahk in the 'lib' folder and a call to updates() at the top of your main script
; Required includes: func_DebugFunctions.ahk, func_FileMap.ahk, func_GetCSIDL.ahk,
;				func_FileGetVersionInfo.ahk
; Functions should be declared as: <library_name>(pn="", por="")
; On FIRST call, pn can be either an API name, Ordinal name (OrdinalX) or '+' to dump the API list to file.
; Also on FIRST Call, the optional parameter por can pass a path to a specific version of a library.
; When finished using such library, it must be unloaded and memory freed by calling the function with no parameters.
;================================================================
Static
Global AW, Ptr, AStr
if !pn
	{
	Loop, Parse, nameList, `n, `r
		VarSetCapacity(v%A_LoopField%, 0)
	Loop, Parse, ordsList, `n, `r
		VarSetCapacity(o%A_LoopField%, 0)
	if hLib
		DllCall("FreeLibrary", Ptr, hLib)
	VarSetCapacity(hLib, 0)
	return
	}
if !hLib
	{
	if por
		Loop, %por%
			por := A_LoopFileLongPath				; Convert any relative path to absolute path
	lib := por ? por : GetCSIDL(0x25) "\" A_ThisFunc ".dll"	; Allow path override for libraries
	if !hLib := MapFile(lib, 1)
		return -1
	if !inth := ImageNtHeader(hLib)
		return -2
	IED := ImageDirectoryEntryToData(hLib, 0, desz, 0)	; IMAGE_EXPORT_DIRECTORY table
	Ename := NumGet(IED+0, 3*4, "UInt")				; Name (associated with the exports)
	base := NumGet(IED+0, 4*4, "UInt")				; Base ordinal
	namesN := NumGet(IED+0, 6*4, "UInt")			; NumberOfNames
	ordsN := NumGet(IED+0, 5*4, "UInt")-namesN		; NumberOfFunctions
	names := NumGet(IED+0, 8*4, "UInt")				; AddressOfNames
	ords := NumGet(IED+0, 9*4, "UInt")				; AddressOfNameOrdinals
	Loop, %namesN%
		nameList .= MulDiv(hLib+GetPtrFromRVA(NumGet(ImageRvaToVa(hLib, inth, names+4*(A_Index-1)), 0, "UInt"), inth, hLib), "AStr") "`n"
	Loop, %ordsN%
		ordsList .= "Ordinal" (base+NumGet(ImageRvaToVa(hLib, inth, ords+2*(A_Index-1)), 0, "UShort")) "`n"	; needs fixed !!!
	Sort, nameList, U
	Sort, ordsList, U
	UnmapFile(1)
	if (pn="+")
		{
		if (filever := FileGetVersionInfo(lib, "FileVersion")) < 0
			filever := "(uknver)"
		outfile := "APIs\" A_ThisFunc "-" filever "-" A_ComputerName ".txt"
		IfNotExist, APIs
			FileCreateDir, APIs
		else FileDelete, %outfile%
		list := (nameList ? "API Names:`n" nameList : "")
			. (ordList ? (nameList ? "`n" : "") "Ordinals:`n" ordsList : "")
		FileAppend, %list%, %outfile%
		OutputDebug, � Library %lib%`nAPIs dumped in`n%outfile%
		}
	if !hLib := DllCall("LoadLibrary" AW, "Str", lib, Ptr)
		return -3
	ofi := A_FormatInteger
	SetFormat, Integer, H
	Loop, Parse, nameList, `n, `r
		v%A_LoopField% := DllCall("GetProcAddress", Ptr, hLib, AStr, A_LoopField, Ptr)
	VarSetCapacity(nameList, 0)
	Loop, Parse, ordsList, `n, `r
;		v%A_LoopField% := DllCall("GetProcAddress", Ptr, hLib, AStr, A_LoopField, Ptr)
		{
		StringReplace, o, A_LoopField, Ordinal,,
		o+=0
		v%A_LoopField% := DllCall("GetProcAddress", Ptr, hLib, "UInt", o, Ptr)
		}
	VarSetCapacity(ordsList, 0)
	SetFormat, Integer, %ofi%
	}
if (pn="+")
	return
else if v%pn%
	return v%pn%
OutputDebug, % A_ThisFunc "() -> " pn ": No procedure address found."
return -4
;================================================================
