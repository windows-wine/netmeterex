; � Drugwash, May 2016	v1.0
;================================================================
GetWinVisible(hwnd)
;================================================================
{
Global Ptr
vis := DllCall("IsWindowVisible", Ptr, hwnd)
VarSetCapacity(wps, sz := 44, 0)
NumPut(sz, wps, 0, "UInt")
DllCall("GetWindowPlacement", Ptr, hwnd, Ptr, &wps)
st := NumGet(wps, 8, "UInt")	; 1=SW_SHOWNORMAL, 2=SW_SHOWMINIMIZE
; Result: 0=hidden, 1=visible, 2=minimized, -1=other (see SW_* values)
res := (st=1 && !vis) ? 0 : (st=1 && vis) ? 1 : (st=2 && vis) ? 2 : -1
return res
}
