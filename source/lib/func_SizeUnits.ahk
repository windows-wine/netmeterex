; � Drugwash 2016, v1.4
; Add 's' to return bits instead of bytes, 'i' to compute binary thousands (1024) instead of decimal (1000)
;================================================================
SizeUnits(val, ByRef unit, d="")	; (A)uto/(b)ytes/(k)ilo/(M)ega/(G)iga/(T)era/(E)xa/(P)eta(i)(s)|(0)x.y decimals
;================================================================
{
Static t="BkMGTEP"
if (val="")
	{
	unit=
	ErrorLevel := 1
	return
	}
off = %A_FormatFloat%
if InStr(unit, "i")					; select 'power of two' or 'power of ten' unit
	{
	u2 := 1024
	StringReplace, unit, unit, i,, All
	i := "i"
	}
else u2 := 1000, i := ""
if (val < u2)
	{
	unit := InStr(unit, "s") ? "b" : "B"
	return Round(val)
	}
StringLeft, u1, unit, 1
d := d ? d : off
oat := A_AutoTrim
AutoTrim, Off
; Doesn't obey leading zeroes format for floating numbers!
SetFormat, Float, %d%
Loop, Parse, t
	if ((u1="A" && val < u2**A_Index) OR (u1 = A_LoopField))
		{
		r := val/u2**(A_Index-1)
		unit := A_LoopField i (InStr(unit, "s") ? "b" : "B")
		SetFormat, Float, %off%
		AutoTrim, %oat%
		return r
		}
AutoTrim, %oat%
ErrorLevel := 1
}
