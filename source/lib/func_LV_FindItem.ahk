; originally implemented in LCW as LV_FindStr()
;================================================================
LV_FindItem(hLV, strg, ex=0)
;================================================================
{
Global Ptr, AW
ps := A_PtrSize ? A_PtrSize : 4
VarSetCapacity(LVFI, 20+ps, 0)		; LVFINDINFO struct
NumPut(ex ? 2 : 10, LVFI, 0, "UInt")	; LVFI_STRING +/LVFI_PARTIAL
NumPut(&strg, LVFI, 4, Ptr)			; psz
SendMessage, % (A_IsUnicode ? 0x1053 : 0x100D), -1, &LVFI,, ahk_id %hLV%	; LVM_FINDITEMA/W
return 1+(ErrorLevel <<32 >> 32)
}
