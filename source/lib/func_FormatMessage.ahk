; #include updates.ahk
;================================================================
FormatMessage(ctx, msg, arg="")
;================================================================
{
Global
Local txt, buf
SetFormat, Integer, H
msg+=0
SetFormat, Integer, D
DllCall("FormatMessage" AW
	, "UInt",	0x1100		; FORMAT_MESSAGE_FROM_SYSTEM/ALLOCATE_BUFFER
	, "UInt",	0			; lpSource
	, "UInt",	msg			; dwMessageId
	, "UInt",	0			; dwLanguageId (0x0418=RO)
	, PtrP,	buf			; lpBuffer
	, "UInt",	0			; nSize
	, "Str",	arg)			; Arguments
txt := DllCall("MulDiv", Ptr, buf, "Int", 1, "Int", 1, "Str")
DllCall("LocalFree", Ptr, buf)
return "Error " msg " in " ctx ":`n" txt
}
