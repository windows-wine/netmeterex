; � Drugwash 2016, v1.1 incomplete
;#include updates.ahk
; updates()

GetDurationFormat(nmb, lcid=0)
{
Global Ptr,A_CharSize
/*
sz := DllCall("GetDurationFormat"		; Vista+
	, "UInt", lcid			; LCID
	, Ptr, &nmb			; lpDuration
	, Int64, altD			; ullDuration
	, Ptr, &fmt			; lpFormat
	, Ptr, &out			; lpDurationStr
	, "UInt", sz := 0)		; cchDuration
if !sz
	return, ErrorLevel := 1
VarSetCapacity(out, sz*A_CharSize, 0)
if !DllCall("GetDurationFormat"
	, "UInt", lcid			; LCID
	, Ptr, &nmb			; lpDuration
	, Int64, altD			; ullDuration
	, Ptr, &fmt			; lpFormat
	, Ptr, &out			; lpDurationStr
	, "UInt", sz)			; cchDuration
	return, ErrorLevel := 1
VarSetCapacity(out, -1)
*/
return out
}

GetDurationFormat2(val, p="s", f=FALSE)
{
Static prec := "ywdhmsi", un := "year|years,week|weeks,day|days,hour|hours,minute|minutes,second|seconds,millisecond|milliseconds"
if !InStr(prec, p)
	return -1, ErrorLevel := 1
Vi := SubStr("00" . Floor((val - Floor(val//1000))*1000), -2)	; milliseconds
val //= 1000
Vs := SubStr("0" . Floor(Mod(val, "60")), -1)				; seconds
Vm := SubStr("0" . Floor(Mod(val//60, "60")), -1)			; minutes
Vh := SubStr("0" . Floor(Mod(val//3600, "24")), -1)		; hours
Vd := SubStr(Floor(Mod(val//86400, "7")), 1)			; days
Vw := SubStr(Floor(Mod(val//604800, "52")), 1)			; weeks
Vy := SubStr(Floor(Mod(val//31449600, "99")), 1)		; years
Loop, Parse, un, CSV
	{
	t := SubStr(prec, A_Index, 1)
	if (!V%t% && A_Index < 4)
		continue
	StringSplit, u, A_LoopField, |
	unit := V%t% = 1 ? u1 : u2
	r .= V%t% (A_Index<4 ? " " (f ? unit : t) " " : (t="s" ? "." : ":"))
	if (p=t)
		break
	}
StringTrimRight, r, r, 1
return r
}
