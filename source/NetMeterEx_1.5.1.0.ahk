; http://www.autohotkey.com/forum/viewtopic.php?p=114703#114703
; http://autohotkey.com/board/topic/16574-network-downloadupload-meter/
; rewrite by Drugwash 2016-2019
;================================================================
;@Ahk2Exe-Obey bits, = %A_PtrSize% * 8
;@Ahk2Exe-Obey xbit, = "%A_PtrSize%" > 4 ? "x64" : "x86"
;@Ahk2Exe-Obey type, = "%A_IsUnicode%" ? "Unicode" : "ANSI"
;@Ahk2Exe-Obey xtype, = "%A_IsUnicode%" ? "W" : "A"
;@Ahk2Exe-Let version = 1.5.1.0
;@Ahk2Exe-AddResource %A_ScriptDir%\res\fullstrip-16.bmp, 100
;@Ahk2Exe-AddResource %A_ScriptDir%\res\fullstrip-32.bmp, 101
;@Ahk2Exe-AddResource %A_ScriptDir%\res\fullstrip-48.bmp, 102
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\simple NN 1632.ico, 159
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\simple NN 1632.ico, 207
;@Ahk2Exe-SetFileVersion %U_version%
;@Ahk2Exe-SetProductVersion 1.5.0.0
;@Ahk2Exe-SetProductName NetMeterEx
;@Ahk2Exe-SetInternalName NetMeterEx %U_version%.ahk
;@Ahk2Exe-SetOrigFilename NetMeterEx.exe
;@Ahk2Exe-SetDescription NetMeterEx - Monitors selected network connection
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetCopyright Copyright @ Drugwash`, 2016-Jan 2022
;@Ahk2Exe-SetLegalTrademarks Released under the terms of the GNU General Public License
;@Ahk2Exe-Set Comments, Compiled in AHK %A_AhkVersion% %U_type% %U_bits%bit.
;@Ahk2Exe-SetMainIcon %A_ScriptDir%\res\simple NN 1632.ico
;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.NetMeterEx, %U_version%
;================================================================
#NoEnv
#Persistent
#SingleInstance, Off			; We may want to monitor multiple network interfaces at the same time
ListLines, Off
CoordMode, Tooltip, Screen
SetControlDelay, -1
SetWinDelay, -1
SetBatchLines, -1
SetFormat, Float, 01.1
SetFormat, Integer, D
DetectHiddenWindows, On
;if IsFunc(k := "DebugBIF")		; This one interferes with DbgView, crashes script
;	%k%()					; if launched while debugger is running
;DebugBIF()
;================================================================
appname=NetMeterEx
comment=Monitors selected network connection
author=Drugwash
version=1.5.1.0
releaseD=January 23, 2022
releaseT=public beta
appenc := A_IsUnicode ? "Unicode" : "ANSI"
iconlocal=%A_ScriptDir%\res\simple NN 1632.ico
debug=0
;================================================================
SetWorkingDir, %A_ScriptDir%
if !updates()
	{
	MsgBox, 0x2015, %appname% error,
		(LTrim
		A required piece of script is missing: updates.ahk .
		Please drop the file in your AHK's 'lib' folder or
		#include it before this call. Once done that, retry.
		)
	IfMsgBox Retry
		reload
	ExitApp
	}

w1x := A_ScreenWidth-64, w1y := 40, w2x := "Center", w2y := "Center"
; black rhomb, arrow up, arrow down, black circle, squared X, black square, white square
symbols := "74,E9,EA,6C,78,6E,6F"
Loop, Parse, symbols, CSV
	c%A_Index% := "0x" (A_IsUnicode ? "F0" : "") A_LoopField
; None, Send, Receive, Send/Receive
Iss1 := "1,7,4,14"	; redred
Iss2 := "1,7,3,13"	; redgreen
Iss3 := "1,7,2,12"	; redblue
Iss4 := "1,6,3,11"	; greengreen
Iss5 := "1,6,2,10"	; greenblue
Iss6 := "1,5,3,9"	; bluegreen
Iss7 := "1,5,2,8"	; blueblue
Iso := "16,17,18,19,20,21,22"	; shared,default,disconnected,error,LAN,phone,GSM overlays

hIL1 := ILC_List("16", (A_isCompiled ? A_ScriptFullPath : "res\fullstrip-16.bmp"), "100", 0)
hIL2 := ILC_List("32", (A_isCompiled ? A_ScriptFullPath : "res\fullstrip-32.bmp"), "101", 0)
hIL3 := ILC_List("48", (A_isCompiled ? A_ScriptFullPath : "res\fullstrip-48.bmp"), "102", 0)
DllCall("ImageList_SetOverlayImage", Ptr, hIL1, "UInt", 14, "UInt", 1)
DllCall("ImageList_SetOverlayImage", Ptr, hIL1, "UInt", 15, "UInt", 2)
DllCall("ImageList_SetOverlayImage", Ptr, hIL1, "UInt", 16, "UInt", 3)

iNo := prv_iNo := 4
stMin := w9x ? "1" : "4"	; Minimum operating status
stDesc := "Disabled,WAN not connected,Disconnected / No carrier,Connecting,Connected,Connected"
StringSplit, stDesc, stDesc, `,
badStat := "023"			; bad status modes
states := "CLOSED,LISTENING,SYN_SENT,SYN_RECEIVED,ESTABLISHED,FIN_WAIT1,FIN_WAIT2
	,CLOSE_WAIT,CLOSING,LAST_ACK,TIME_WAIT,DELETE_TCB"
StringSplit, cState, states, `,
; We need to tell apart network interfaces with their own settings.
; This may require a generic ini file containing 'lastChoice' and 'autoLast',
; and several others, one for each used interface.
inifile := appname "_preferences.ini"
logfile := appname "_traffic.txt"
tmpdir := A_Temp "\" appname
sessTfile := appname "_conn_" A_Now ".txt"
logPathS := A_AppData "\" appname "\logs"
logPathP := A_ScriptDir "\logs"
if FileExist(setf := A_AppData "\" appname "\" inifile)
	{
	IniRead, setL, %setf%, Settings, SetupLocation, 2
;	setL=2
	}
else if FileExist(setf := A_ScriptDir "\" inifile)
	{
	IniRead, setL, %setf%, Settings, SetupLocation, 3
;	setL=3
	}
else
	{
	RegRead, setL, HKLM, Software\Drugwash\%appname%, SetupLocation
	setL=1
	setf=
	}
if setL>1		; we have ini file
	{
	setf := setL=3 ? A_ScriptDir "\" inifile : A_AppData "\" appname "\" inifile
IniRead, X1, %setf%, GuiPos, XPos, %w1x%
IniRead, Y1, %setf%, GuiPos, YPos, %w1y%
IniRead, X2, %setf%, GuiPos, XPos2, %w2x%
IniRead, Y2, %setf%, GuiPos, YPos2, %w2y%
IniRead, setT, %setf%, Settings, SetupType, 2		; Setup type (static/portable/custom)
IniRead, color, %setf%, Settings, ColorScheme, 3	; Select red/blue color scheme by default
IniRead, showHints, %setf%, Settings, ShowHints, 1	; Show arrow hints near icon in floating window
IniRead, noAE, %setf%, Settings, NoAutoexpand, 0	; Don't expand floating window on hover
IniRead, all, %setf%, Settings, ShowAll, 1			; Show ALL interfaces, including disconnected ones
IniRead, lastChoice, %setf%, Settings, LastChoice, %A_Space%	; Get last chosen interface
IniRead, autoLast, %setf%, Settings, AutoloadLastChoice, 1	; Use last chosen interface automatically
IniRead, fwm, %setf%, Settings, MainWinLastState, 1		; Floating window last state
IniRead, pin, %setf%, Settings, MainWinPinned, 0			; Floating window pinned state
IniRead, fws, %setf%, Settings, MainWinStartState, 2		; Floating window startup mode
IniRead, logPathC, %setf%, Settings, LogPathC, .\logs		; Log path (custom)
IniRead, logSess, %setf%, Settings, LogSessions, 1			; Log sessions
IniRead, logTraf, %setf%, Settings, LogTraffic, 0			; Log connections
IniRead, logLTraf, %setf%, Settings, LogLANTraffic, 0		; Log LAN connections
IniRead, maxLLsize, %setf%, Settings, MaxLANLogSize, 4		; Max LAN log size [MBi]
IniRead, maxTSlen, %setf%, Settings, MaxTrafficLength, 4096	; Max traffic chunk before writing to disk
IniRead, mRate, %setf%, Settings, MeasuringRate, .5		; Traffic reading interval [s]
IniRead, unit, %setf%, Settings, Unit, 1000					; Traffic measure unit (1000/1024 bytes/kb)
	}
else
	{
	; to do registry reading
	; bugfix: GUI cannot initialize when ini file is missing
	RegRead, X1, HKLM, Software\Drugwash\%appname%, XPos
	if (X1 = "")	; registry has not been initialized
		{
		X1 := w1x, Y1 := w1y, X2 := w2x, Y2 := w2y, setT := 2, color := 3, showHints := 1, noAE := "0"
		all := 1, lastChoice := "", autoLast := 1, fwm := 1, pin := "0", fws := 2, logPathC := ".\logs"
		logSess := 1, logTraf := "0", logLTraf := "0", maxLLsize := 4, maxTSlen := 4096, mRate := .5
		unit := 1000
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, XPos, %X1%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, YPos, %Y1%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, XPos2, %X2%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, YPos2, %Y2%
	
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, SetupType, %setT%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ColorScheme, %color%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ShowHints, %showHints%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ShowAll, %all%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LastChoice, %choice%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, AutoloadLastChoice, %autoLast%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MainWinPinned, % (pin ? 1 : 0)
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogSessions, %logSess%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogTraffic, %logTraf%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogLANTraffic, %logLTraf%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MaxTrafficLength, %maxTSlen%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MeasuringRate, %mRate%
	RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, Unit, %unit%
	}
	else
		{
		RegRead, X1, HKLM, Software\Drugwash\%appname%, XPos
		RegRead, Y1, HKLM, Software\Drugwash\%appname%, YPos
		RegRead, X2, HKLM, Software\Drugwash\%appname%, XPos2
		RegRead, Y2, HKLM, Software\Drugwash\%appname%, YPos2
		
		RegRead, setT, HKLM, Software\Drugwash\%appname%, SetupType
		RegRead,color , HKLM, Software\Drugwash\%appname%, ColorScheme
		RegRead, showHints, HKLM, Software\Drugwash\%appname%, ShowHints
		RegRead, all, HKLM, Software\Drugwash\%appname%, ShowAll
		RegRead, choice, HKLM, Software\Drugwash\%appname%, LastChoice
		RegRead, autoLast, HKLM, Software\Drugwash\%appname%, AutoloadLastChoice
		RegRead, pin, HKLM, Software\Drugwash\%appname%, MainWinPinned
		RegRead, logSess, HKLM, Software\Drugwash\%appname%, LogSessions
		RegRead, logTraf, HKLM, Software\Drugwash\%appname%, LogTraffic
		RegRead, logLTraf, HKLM, Software\Drugwash\%appname%, LogLANTraffic
		RegRead, maxTSlen, HKLM, Software\Drugwash\%appname%, MaxTrafficLength
		RegRead,mRate , HKLM, Software\Drugwash\%appname%, MeasuringRate
		RegRead, unit, HKLM, Software\Drugwash\%appname%, Unit
		}
	}
logPath := setT="1" ? logPathS : setT="2" ? logPathP : logPathC
Ics1 := Item(Iss%color%, 4)	; Send/Receive
Ics2 := Item(Iss%color%, 2)	; Send
Ics3 := Item(Iss%color%, 3)	; Receive
Ics4 := Item(Iss%color%, 1)	; None

Menu, Tray, UseErrorLevel
Menu, Tray, Tip, %appname%
if !A_IsCompiled
	Menu, Tray, Icon, %iconlocal%
Menu, Tray, NoIcon
Menu, Tray, Click, 1
Menu, Tray, NoStandard
Menu, Tray, Add, Show/Hide, ShowHide
Menu, Tray, Add, Status, showStat
Menu, Tray, Add
Menu, Tray, Add, Switch interfaces, start
Menu, Tray, Add, Settings, settings
Menu, Tray, Add
Menu, Tray, Add, Read session log, readLog
Menu, Tray, Add, Read traffic logs, readTLog
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Pause, pause
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, exit
Menu, Tray, Default, Status
;===== MAIN FLOATING WINDOW =====
Gui, +ToolWindow -Caption +Border +AlwaysOnTop +LastFound +OwnDialogs +Theme
hMain := WinExist()
Gui, Margin, 1, 1
Gui, Color, White, White
;Gui, Color, ECFFFF, ECFFFF
Gui, Font, s8, Wingdings
Gui, Add, Text, x2 y2 w11 h11 +0x200 hwndhPin vbPin gpin, % Chr(c6)
Gui, Add, Text, x2 y14 w10 h10 +0x200 +Center vhint, % Chr(c2)
if !showHints
	GuiControl, +Hidden, hint
np := pin ? 0 : 1				; account for (un)pinned startup
Gui, Font, s7 w400, Tahoma
Gui, Add, Picture, x14 y1 w32 h32 +0x10E AltSubmit BackgroundTrans hwndhPic2,
Gui, Add, Text, x1 y36 w50 h11 +0x200 +Center vmws0, 888.88MB/s
Gui, Add, Text, x1 y48 w50 h11 +0x200 +Center Hidden%np% vmws2, 888.88MB/s
Gui, Add, Text, x54 y1 w102 h12 +0x200 +Right Hidden%np% vmws3, 888.888.888.888 bytes
Gui, Add, Text, x54 y13 w102 h12 +0x200 +Right Hidden%np% vmws4, 888.888.888.888 bytes
Gui, Add, Text, x54 y25 w50 h11 +0x200 +Center Hidden%np% vmws5, Max speed:
Gui, Add, Text, x54 y36 w50 h11 +0x200 +Center Hidden%np% vmws6, 888.88MB/s
Gui, Add, Text, x54 y48 w50 h11 +0x200 +Center Hidden%np% vmws7, 888.88MB/s
Gui, Add, Text, x106 y25 w50 h11 +0x200 +Center Hidden%np% vmws8, Average:
Gui, Add, Text, x106 y36 w50 h11 +0x200 +Center Hidden%np% vmws9, 888.88MB/s
Gui, Add, Text, x106 y48 w50 h11 +0x200 +Center Hidden%np% vmws10, 888.88MB/s
;===== STATUS WINDOW =====
Gui, 2:+LastFound -MinimizeBox -MaximizeBox +E0x400 +Theme
Gui, 2:Font, s8 w400, Tahoma
hStat := WinExist()
t := A_AhkVersion >= "1.1.24" ? 3 : ""
Gui, 2:Add, Tab%t%, x5 y6 w348 h375 +Theme -Background 0x04030240 E0x4 vtab
	, General|Support|Details
; TAB General
Gui, 2:Tab, 1
Gui, 2:Add, GroupBox, x20 y39 w320 h141 +Theme E0x4, Connection
Gui, 2:Add, Text, x35 y61 w35 h13 BackgroundTrans, Status:
Gui, 2:Add, Text, x119 y61 w206 h13 BackgroundTrans Right vstDesc,
Gui, 2:Add, Text, x35 y80 w45 h13 BackgroundTrans, Duration:
Gui, 2:Add, Text, x119 y80 w206 h13 BackgroundTrans Right vdur,
Gui, 2:Add, Text, x35 y100 w36 h13 BackgroundTrans, Speed:
Gui, 2:Add, Text, x119 y100 w206 h13 BackgroundTrans Right vdspeed,
Gui, 2:Add, Text, x35 y120 w290 h50 BackgroundTrans Center vdbg,
Gui, 2:Add, GroupBox, x20 y187 w320 h141 +Theme E0x4, Activity
Gui, 2:Add, Text, x90 y220 w62 h13 BackgroundTrans Right, Sent
Gui, 2:Add, Text, x158 y226 w20 h2 BackgroundTrans 0x8 E0x4, 
Gui, 2:Add, Picture, x191 y208 w48 h48 +0x10E AltSubmit BackgroundTrans hwndhPic3,
Gui, 2:Add, Text, x243 y226 w20 h2 BackgroundTrans 0x8 E0x4, 
Gui, 2:Add, Text, x270 y220 w53 h13 BackgroundTrans Right, Received
Gui, 2:Add, Text, x35 y267 w51 h13 BackgroundTrans 0x200 vtctl8, Packets:
Gui, 2:Add, Text, x77 y267 w105 h13 BackgroundTrans Right 0x200 vu0,
Gui, 2:Add, Text, x210 y267 w2 h16 BackgroundTrans 0x8 E0x4 vtctl0, 
Gui, 2:Add, Text, x220 y267 w105 h13 BackgroundTrans Right 0x200 vd0,

Gui, 2:Add, Text, x35 y286 w65 h13 BackgroundTrans 0x200 vtctl1, Compression:
Gui, 2:Add, Text, x77 y286 w105 h13 BackgroundTrans Right 0x200 vtctl2,
Gui, 2:Add, Text, x220 y286 w105 h13 BackgroundTrans Right 0x200 vtctl3,
Gui, 2:Add, Text, x35 y304 w48 h13 BackgroundTrans 0x200 vtctl4, Errors:
Gui, 2:Add, Text, x77 y304 w105 h13 BackgroundTrans Right 0x200 vtctl5, 0
Gui, 2:Add, Text, x220 y304 w105 h13 BackgroundTrans Right 0x200 vtctl6, 0

Gui, 2:Add, Button, x20 y340 w75 h23, &Properties
Gui, 2:Add, Button, x99 y340 w83 h23 vtctl7 gdisable, &Disable
; TAB Support
Gui, 2:Tab, 2
Gui, 2:Add, GroupBox, x20 y39 w320 h156 +Theme E0x4, Connection status
Gui, 2:Add, Picture, x32 y62 w32 h32 0x10E AltSubmit BackgroundTrans hwndhPic1,
Gui, 2:Add, Text, x74 y62 w83 h13 BackgroundTrans, Address Type:
Gui, 2:Add, Text, x144 y62 w180 h13 BackgroundTrans Right vt2ctl1,	; Assigned by DHCP
Gui, 2:Add, Text, x74 y87 w56 h13 BackgroundTrans, IP Address:
Gui, 2:Add, Text, x144 y87 w180 h13 BackgroundTrans Right vt2ctl2,
Gui, 2:Add, Text, x74 y111 w68 h13 BackgroundTrans, Subnet Mask:
Gui, 2:Add, Text, x144 y111 w180 h13 BackgroundTrans Right vt2ctl3,
Gui, 2:Add, Text, x74 y135 w94 h13 BackgroundTrans, Default Gateway:
Gui, 2:Add, Text, x174 y135 w150 h13 BackgroundTrans Right vt2ctl4, 
Gui, 2:Add, Button, x74 y160 w83 h23 gshow3, &Details...
Gui, 2:Add, Text, x24 y212 w219 h57 BackgroundTrans
	, Windows did not detect problems with this connection. If you cannot connect`, click Repair.
Gui, 2:Add, Button, x255 y212 w83 h23, Re&pair
; TAB Details
Gui, 2:Tab, 3
Gui, 2:Add, ListView, x20 y39 w320 h328 NoSort AltSubmit +Theme -LV0x30, Property|Value
Gui, 2:Tab
Gui, 2:Add, Button, x279 y387 w75 h23, &Close
s := "Device Name,Device Type,Server Type,Transports,Authentication,Compression,PPP multilink framing
	,Server IP address,Client IP address"
Loop, Parse, s, CSV
	LV_Add("", A_LoopField)
LV_ModifyCol(1, "AutoHdr")
LV_ModifyCol(2, "AutoHdr")
;===== DETAILS WINDOW =====
Gui, 3:+Owner2 -MinimizeBox -MaximizeBox +E0x400 +LastFound
hDet := WinExist()
Gui, 3:Margin, 10, 11
Gui, 3:Font, s7 w700, Tahoma
Gui, 3:Add, Text, x11 y11 w320 h13 +0x4000, Network Connection &Details:
Gui, 3:Font, s8 w400, Tahoma
Gui, 3:Add, ListView, x11 y28 w320 h216, Property|Value
Gui, 3:Add, Text, x11 y280 w320 h2 0x10, 
Gui, 3:Add, Button, x255 y288 w75 h23, &Close
;===== INTERFACE SELECTION DIALOG =====
Gui, 4:+AlwaysOnTop +Caption +LastFound +Resize +Theme +MinSize460x210
Gui, 4:Margin, 2, 2
Gui, 4:Add, Text, x2 y2 w460 h30 +Center,
	(LTrim
	We found %adCnt% network interfaces on this machine.
	Please select the one to be monitored:
	)
Gui, 4:Add, ListView, x2 y34 w460 h250 -0x4000000 AltSubmit hwndhLV4 gsel4
	, Idx|Adapter name|Type|Type name|Details|Status|Status name|Real index|?
Gui, 4:Add, Text, x2 y286 w70 h14 +0x200 +Right, Type:
Gui, 4:Add, Text, x2 y300 w70 h14 +0x200 +Right, Details:
Gui, 4:Add, Text, x2 y314 w70 h14 +0x200 +Right, Status:
Gui, 4:Add, Text, x2 y328 w70 h14 +0x200 +Right, Real index:
Gui, 4:Add, Edit, x74 y286 w388 h14 -Multi -E0x200 +ReadOnly vit1, 
Gui, 4:Add, Edit, x74 y300 w388 h14 -Multi -E0x200 +ReadOnly vit2, 
Gui, 4:Add, Edit, x74 y314 w388 h14 -Multi -E0x200 +ReadOnly vit3, 
Gui, 4:Add, Edit, x74 y328 w388 h14 -Multi -E0x200 +ReadOnly vit4, 
Gui, 4:Add, Button, x2 y346 w100 h30, OK
Gui, 4:Add, Button, x106 y346 w100 h30, Cancel
Gui, 4:Add, Button, x342 y346 w100 h30, Exit
Gui, 4:Default
Loop, 9
	LV_ModifyCol(A_Index, A_Index > 2 ? 0 : "AutoHdr")
LV_ModifyCol(1, "AutoHdr Number Right")
Gui, 4:Show, Center w460 h210 Hide, %appname% interface selection

; Generated using SmartGuiXP Creator mod 4.3.29.8
Gui, 1:Default
Gui, Show, x%X1% y%Y1% AutoSize Hide, %appname%
Gui, 2:Show, x%X2% y%Y2% w362 h418 Hide, %appname% status
VarSetCapacity(s, 0)
hCursM := DllCall("LoadCursor" AW, Ptr, NULL, "Int", 32646, Ptr)	; IDC_SIZEALL
hCursH := DllCall("LoadCursor" AW, Ptr, NULL, "Int", 32649, Ptr)	; IDC_HAND
WPNew := RegisterCallback("WindowProc", "", 4, "")
WPOld1 := DllCall("SetWindowLong", Ptr, hPic1, "Int", -4, Ptr, WPNew, Ptr)
WPOld2 := DllCall("SetWindowLong", Ptr, hPic2, "Int", -4, Ptr, WPNew, Ptr)
WPOld3 := DllCall("SetWindowLong", Ptr, hPic3, "Int", -4, Ptr, WPNew, Ptr)
if !w9x
	SetTrayIcon(hMain, 1, "v3")	; NOTIFYICON_VERSION
SetTrayIcon(hMain, 1, "i" hIL1 "|1|1,m0x1966,tReady")
OnMessage(0x1966, "traymsg")
; add back tray icon on shell restart
OnMessage(DllCall("RegisterWindowMessage", "Str", "TaskbarCreated", "UInt"), "retray")
OnMessage(0xA0, "winexp")		; WM_NCMOUSEMOVE
OnMessage(0x200, "winexp")		; WM_MOUSEMOVE
OnMessage(0x201, "winmove")	; WM_LBUTTONDOWN
OnMessage(0xA1, "notrk")		; WM_NCLBUTTONDOWN
OnMessage(0x2A3, "wincol")		; WM_MOUSELEAVE
OnMessage(0x53, "help")		; WM_HELP
OnExit, cleanup
WSAStartup()
;================================================================
start:
;================================================================
if !r := GetIfTable(tb, 1)
	{
	err := FormatMessage("GetIfTable()", r)
	MsgBox, 0x2040, %appname% info, Can't retrieve interfaces table.`n%err%`n`nApplication will exit.
	ExitApp
	}
if !adCnt := NumGet(tb, 0, "UInt")
	{
	MsgBox, 0x2040, %appname% info, No network interfaces found.`nApplication will exit.
	ExitApp
	}
IfNotExist, %tmpdir%\atypes.txt
	{
	FileCreateDir, %tmpdir%
	FileInstall, res\atypes.txt, %tmpdir%\atypes.txt
	FileInstall, res\ctxhlp.ini, %tmpdir%\ctxhlp.ini
	}
FileRead, at, %tmpdir%\atypes.txt
ctxhlp := tmpdir "\ctxhlp.ini"

Gui, 4:Default
GuiControl, 4:, Static1,
	(LTrim
	We found %adCnt% network interfaces on this machine.
	Please select the one to be monitored:
	)
LV_Delete()
Loop, %adCnt%
	{
	s := NumGet(tb, 4 + 860 * (A_Index - 1) + 544, "UInt")+1	; dwOperStatus
	t := NumGet(tb, 4 + 860 * (A_Index - 1) + 516, "UInt")	; dwType
	if !all && (s < stMin+1 || t=24)
		Continue

	pntr:= &tb + 4 + 860 * (A_Index - 1)					; table pointer
	chI := NumGet(pntr+0, 512, "UInt")					; dwIndex
	c := chI & 0xFFFF									; dwIndex (short type for display)
	stAdm := NumGet(pntr+0, 540)						; dwAdminStatus
	at2 := at3 := ""
	Loop, Parse, at, `n, `r
		if InStr(A_LoopField, t A_Tab)=1
			{
			StringSplit, at, A_LoopField, %A_Tab%			; adapter type details
			break
			}
	a := MulDiv(pntr+604, "AStr")						; adapter description
	a=%a%											; clean up unwanted whitespace
	ri := d2s(chI)
; 	Idx|Adapter name|Type|Type name|Details|Status|Status name|Real index|?
	LV_Add("", c, a, t, at2, at3, s, stDesc%s%, chI)
	}
Loop, 9
	LV_ModifyCol(A_Index, A_Index > 2 ? 0 : "AutoHdr")
if (autoLast && !inSession)
	{
	choice := lastChoice
	inSession=1
	if (ir := LV_FindItem(hLV4, choice, TRUE))	; Search for an exact match
		{
		LV_GetText(info1, ir, 3)		; atype
		LV_GetText(chIdx, ir, 8)
		Gui, 1:Default
		sessTfile := appname "_conn_" A_Now ".txt"
		goto autoSelect
		}
	}
;================================================================
select:
Gui, 4:Default
if (ir := LV_FindItem(hLV4, lastChoice, TRUE))	; Search for an exact match
	LV_Modify(ir, "Vis Select Focus")
Gui, 4:Show
return
;================================================================
4GuiSize:
;================================================================
GuiControl, 4:Move, Button3, % "y" A_GuiHeight-32 " x" A_GuiWidth-122
GuiControl, 4:Move, Button2, % "y" A_GuiHeight-32
GuiControl, 4:Move, Button1, % "y" A_GuiHeight-32
GuiControl, 4:MoveDraw, Edit4, % "y" A_GuiHeight-4-14-32 " w" A_GuiWidth-76
GuiControl, 4:MoveDraw, Edit3, % "y" A_GuiHeight-4-2*14-32 " w" A_GuiWidth-76
GuiControl, 4:MoveDraw, Edit2, % "y" A_GuiHeight-4-3*14-32 " w" A_GuiWidth-76
GuiControl, 4:MoveDraw, Edit1, % "y" A_GuiHeight-4-4*14-32 " w" A_GuiWidth-76
GuiControl, 4:MoveDraw, Static5, % "y" A_GuiHeight-4-14-32
GuiControl, 4:MoveDraw, Static4, % "y" A_GuiHeight-4-2*14-32
GuiControl, 4:MoveDraw, Static3, % "y" A_GuiHeight-4-3*14-32
GuiControl, 4:MoveDraw, Static2, % "y" A_GuiHeight-4-4*14-32
GuiControl, 4:Move, syslistview321, % "w" A_GuiWidth-4 " h" A_GuiHeight-(32+4+4*14+2+34)
GuiControl, 4:MoveDraw, Static1, % "w" A_GuiWidth-4
return
;================================================================
sel4:
;================================================================
if !A_EventInfo
	return
ir := A_EventInfo
Loop, 6
	LV_GetText(info%A_Index%, ir, A_Index+2)
GuiControl, 4:, it1, % info1 " (" info2 ")"
GuiControl, 4:, it2, %info3%
GuiControl, 4:, it3, % info4 " (" info5 ")"
GuiControl, 4:, it4, % d2s(info6) " (" info6 ")"
return
;================================================================
4ButtonCancel:
;================================================================
if (!chIdx OR !choice)
	{
Gui, 4:Hide
return
	}
gosub NetMeter
return
;================================================================
4ButtonExit:
;================================================================
ExitApp
;================================================================
4ButtonOK:
;================================================================
if ir
	{
	LV_GetText(choice, ir, 1)
	LV_GetText(chIdx, ir, 8)
	lastChoice := choice
	sessTfile := appname "_conn_" A_Now ".txt"
	}
else return
Gui, 4:Hide
Gui, 1:Default

autoSelect:
Critical
VarSetCapacity(at, 0)										; Free 8kB of memory
if GetInterfaceInfo(buf)	; returns IP_ADAPTER_INDEX_MAP structs
	{
	Loop, %adCnt%	; find the desired interface/adapter and use a pointer to its struct
		{
		if (NumGet(buf, 4+260*(A_Index-1), "UInt")=chIdx)		; Index (infamous IF_INDEX)
			{
			cName := MulDiv(&buf+8+260*(A_Index-1), "Str")	; Name (Not WCHAR in Win9x !)
			break
			}
		}
	}
if info1 in 6,7,11,62,69,117			; ethernet
	{
	atype := 1
	GuiControl, 2:, tab, |General|Support
	GuiControl, 2:MoveDraw, tctl0, h16
	GuiControl, 2:, tctl1, Bytes:
	GuiControl, 2:, tctl7, &Disable
	GuiControl, 2:, tctl8, Packets:
	Loop, 3
		GuiControl, 2:Hide, % "tctl" A_Index+3
	}
else if info1 in 23,48				; modem
	{
	atype := 3
	GuiControl, 2:, tab, |General|Support|Details
	GuiControl, 2:MoveDraw, tctl0, h34
	GuiControl, 2:, tctl7, &Disconnect
	GuiControl, 2:, tctl8, Bytes:
	Loop, 3
		GuiControl, 2:Show, % "tctl" A_Index+3
	}
else if info1 in 24					; loopback
	{
	atype := -1
	GuiControl, 2:, tab, |General|Support
	GuiControl, 2:MoveDraw, tctl0, h16
	GuiControl, 2:, tctl1, Bytes:
	GuiControl, 2:, tctl7, &Disable
	GuiControl, 2:, tctl8, Packets:
	Loop, 3
		GuiControl, 2:Hide, % "tctl" A_Index+3
	}
else
	{
	atype := 4
	GuiControl, 2:, tab, |General|Support
	GuiControl, 2:MoveDraw, tctl0, h16
	GuiControl, 2:, tctl1, Bytes:
	GuiControl, 2:, tctl7, &Disable
	GuiControl, 2:, tctl8, Packets:
	Loop, 3
		GuiControl, 2:Hide, % "tctl" A_Index+3
msgbox, Hm, we're here with interface type=%info1% for item %choice%
	}
overlay2 := Ics%iNo% "," (19+atype)
overlay3 := Ics%iNo% "," (isShared ? "16," : "") (isDefault ? "17," : "") (19+atype)
RepaintWindow(hPic2), RepaintWindow(hPic3)	; Don't wait for traffic to update icons

if (fws =2 OR fws=3 OR (fws=4 && fwm))
	Gui, Show, AutoSize
;IniRead, mRate, %setf%, Settings, MeasuringRate, .5		; [seconds]
;IniRead, unit, %setf%, Settings, Unit, 1000				; [decimal bytes/binary bytes]
mRate := mRate ? mRate : .5
unit := unit ? unit : 1000
ut := unit=1024 ? "i" : ""									; unit type binary/decimal
PollMs := 1000
dnRate := upRate := upMax := dnMax := avgD := avgU := "0", ccs := "6", stat := "unknown"
prv_stat := "", prv_sp := "0"
GetIfEntry(ifrow, chIdx)
dnOld := NumGet(ifrow, 552), upOld  := NumGet(ifrow, 576)	; Old readings for upload/download
FormatTime, CntOn,, yyyy.MM.dd HH:mm:ss
sess := oRead := A_TickCount
Gui, 1:Default		; May be superfluous
SetTrayIcon(hMain, 1, "i" hIL1 "|" Ics4)						; Update tray icon
Sleep, 100			; To avoid blank value in floating window at startup (divide by zero)
isPaused=0
Menu, Tray, Uncheck, Pause
gosub NetMeter
lastDn := dnNew, lastUp := upNew
return
;================================================================
showStat:
;================================================================
Gui, 2:+OwnDialogs
if (!r := GetAdaptersInfo(tmpbuf))
	MsgBox, 0x2010, %appname%, %ErrorLevel%
else
	{
	Gui, 3:Default
	LV_Delete()
	if GetNetworkParams(tmpbuf2)
		{
		dns1 := MulDiv(&tmpbuf2+272, "AStr"), dns2 := ""
		if (i := NumGet(tmpbuf2, 268, "UInt"))						; Next
			dns2 := MulDiv(i+4, "AStr")
		}
	else dns1 := dns2 := "<error in GetNetworkParams()>"
	VarSetCapacity(tmpbuf2, 0)
	seq=0
	Loop, %adCnt%
		{
		ix := NumGet(tmpbuf, 412+seq, "UInt")		; Index
		next := NumGet(tmpbuf, 0+seq, Ptr)			; Next
		if (ix = chIdx)
			break
		seq := next - &tmpbuf
		}
	if (ix != chIdx)
		{
		MsgBox, 0x2010, %appname%,
			(LTrim
			Interface %chIdx% status not available.
			(possibly disconnected or removed)
			)
		return
		}
	desc := MulDiv(&tmpbuf+268+seq, "AStr")						; Description
	desc=%desc%
	GuiControl, 3:, Static1, %desc%:
	s := NumGet(tmpbuf, 400+seq, "UInt")							; AddressLength
	mac := Net_MakeMAC(&tmpbuf+404+seq, s)					; Address
	LV_Add("", "Physical Address", mac)
	isdhcp := NumGet(tmpbuf, 420+seq, "UInt")						; DhcpEnabled
	GuiControl, 2:, t2ctl1, % isdhcp ? "Assigned by DHCP" : "Manually Configured"
	Loop
		{
		i := NumGet(tmpbuf, 428+seq, "UInt")						; IpAddressList.Next
		ip%A_Index% := MulDiv(&tmpbuf+432+seq, "AStr")			; IpAddressList.IpAddress
		mask%A_Index% := MulDiv(&tmpbuf+448+seq, "AStr")		; IpAddressList.IpMask
		LV_Add("", "IP Address", ip%A_Index%)
		LV_Add("", "Subnet Mask", mask%A_Index%)
		if A_Index=1
			{
			GuiControl, 2:, t2ctl2, % ip%A_Index%
			GuiControl, 2:, t2ctl3, % mask%A_Index%
			}
		if !i
			break
		seq += i-&tmpbuf
		}
	Loop
		{
		i := NumGet(tmpbuf, 468+seq, "UInt")						; GatewayList.Next
		gw%A_Index% := MulDiv(&tmpbuf+472+seq, "AStr")			; GatewayList.IpAddress
		LV_Add("", (A_Index=1 ? "Default" : "Secondary") " Gateway", gw%A_Index%)
		if A_Index=1
			GuiControl, 2:, t2ctl4, % gw%A_Index%
		if !i
			break
		seq += i-&tmpbuf
		}
	Loop
		{
		i := NumGet(tmpbuf, 508+seq, "UInt")						; DhcpServer.Next
		d := MulDiv(&tmpbuf+512+seq, "AStr")						; DhcpServer.IpAddress
		dhcp%A_Index% := isdhcp
			? (inet_addr(d)=0xffffffff ? "unreachable" : d) : ""
		if isdhcp
			LV_Add("", "DHCP Server", dhcp%A_Index%)
		if !i
			break
		seq += i-&tmpbuf
		}
	if (isdhcp && ip1 != "0.0.0.0")
		{
		L1 := L2 := "19700101000000"
		diff := A_Now
		diff -= A_NowUTC, s
		L1 += NumGet(tmpbuf, 632+seq, "UInt")+diff, s				; LeaseObtained
		L2 += NumGet(tmpbuf, 636+seq, "UInt")+diff, s				; LeaseExpires
		FormatTime, Lo, %L1%, dd.MM.yyyy HH.mm.ss
		FormatTime, Le, %L2%, dd.MM.yyyy HH.mm.ss
		LV_Add("", "Lease Obtained", Lo)
		LV_Add("", "Lease Expires", Le)
		}
	LV_Add("", "DNS Server", dns1)
	if dns2
		LV_Add("", "DNS Server (2)", dns2)
	if (iswins := NumGet(tmpbuf, 548+seq, "UInt"))					; HaveWins
		{
		LV_Add("", "WINS Server", MulDiv(&tmpbuf+556+seq, "AStr"))		; PrimaryWinsServer
		LV_Add("", "WINS Server (2)", MulDiv(&tmpbuf+596+seq, "AStr"))	; SecondaryWinsServer
		}
	else LV_Add("", "WINS Server", "")							; empty PrimaryWinsServer
	LV_ModifyCol(1, "AutoHdr")
	LV_ModifyCol(2, "AutoHdr")
	}
VarSetCapacity(tmpbuf, 0)
VarSetCapacity(tmpbuf2, 0)
GuiControl, 2:, stDesc, %stat%
GuiControl, 2:, dspeed, %dspeed%ps
Gui, 1:Default
overlay1 := Ics1 "," (19+atype)
;overlay3 := Ics%iNo% "," (isShared ? "16," : "") (isDefault ? "17," : "") (19+atype)
Gui, 2:Show, Restore
return

show3:
Gui, 3:Show, Restore, Network Connection Details
return

2GuiEscape:
2GuiClose:
2ButtonClose:
Gui, 3:Hide
Gui, 2:Hide
return

3GuiEscape:
3GuiClose:
3ButtonClose:
Gui, 3:Hide
return
;================================================================
cleanup:
;================================================================
FormatTime, CntOff,, yyyy.MM.dd HH:mm:ss
if logSess
	gosub logSess
exit=1
if (logTraf && !loggedT)
	gosub flushTraf
iphlpapi()
ws2_32()
WSACleanup()
;DllCall("DestroyCursor", Ptr, hCursM)	; DON'T DESTROY SHARED CURSORS !!!
;DllCall("DestroyCursor", Ptr, hCursH)
ILC_Destroy(hIL1), ILC_Destroy(hIL2), ILC_Destroy(hIL3)
WinGetPos, X1, Y1,,, ahk_id %hMain%
WinGetPos, X2, Y2,,, ahk_id %hStat%
; This needs to be duplicated for registry version
if setL>1		; we have ini file
	{
	gosub saveini
	RegRead, checkIsReg, HKLM, Software\Drugwash\%appname%, SetupType
	if checkIsReg
		gosub savereg
	}
else			; write to registry
	{
	gosub savereg
	IniRead, checkIsIni, %setf%, Settings, SetupType, %A_Space%
	if checkIsIni
		gosub saveini
	}
FileRemoveDir, %tmpdir%, 1
SetTrayIcon(hMain, 1)
Sleep, 200
ExitApp

saveini:
IniWrite, %X1%, %setf%, GuiPos, XPos
IniWrite, %Y1%, %setf%, GuiPos, YPos
IniWrite, %X2%, %setf%, GuiPos, XPos2
IniWrite, %Y2%, %setf%, GuiPos, YPos2

IniWrite, %setT%, %setf%, Settings, SetupType
IniWrite, %setL%, %setf%, Settings, SetupLocation
IniWrite, %color%, %setf%, Settings, ColorScheme
IniWrite, %showHints%, %setf%, Settings, ShowHints
IniWrite, %all%, %setf%, Settings, ShowAll
IniWrite, %choice%, %setf%, Settings, LastChoice
IniWrite, %autoLast%, %setf%, Settings, AutoloadLastChoice
IniWrite, % (pin ? 1 : 0), %setf%, Settings, MainWinPinned
IniWrite, %logSess%, %setf%, Settings, LogSessions
IniWrite, %logTraf%, %setf%, Settings, LogTraffic
IniWrite, %logLTraf%, %setf%, Settings, LogLANTraffic
IniWrite, %maxTSlen%, %setf%, Settings, MaxTrafficLength
IniWrite, %mRate%, %setf%, Settings, MeasuringRate
IniWrite, %unit%, %setf%, Settings, Unit
return

savereg:
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, XPos, %X1%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, YPos, %Y1%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, XPos2, %X2%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, YPos2, %Y2%

RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, SetupType, %setT%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, SetupLocation, %setL%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ColorScheme, %color%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ShowHints, %showHints%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, ShowAll, %all%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LastChoice, %choice%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, AutoloadLastChoice, %autoLast%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MainWinPinned, % (pin ? 1 : 0)
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogSessions, %logSess%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogTraffic, %logTraf%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, LogLANTraffic, %logLTraf%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MaxTrafficLength, %maxTSlen%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, MeasuringRate, %mRate%
RegWrite, REG_SZ, HKLM, Software\Drugwash\%appname%, Unit, %unit%
return

reload:
Reload
Sleep, 700

exit:
ExitApp
;================================================================
about:
;================================================================
MsgBox, 0x2040, About %appname%,
	(LTrim
	%appname% by %author%
	Version %version% %releaseT% (%appenc%)
	Released %releaseD%
	Based on scripts by Sean and SKAN at AutoHotkey forums board
	Some icons by Mark James at http://famfamfam.com/lab/icons/silk/

	This application attempts to provide a network connection indicator
	based on the Windows XP tray icon and status panel style.
	It may prove useful for Windows 98/98SE/ME users as well as
	Vista/7 and maybe later OS versions where such indication is missing.

	Please note the application is not yet complete and may have issues.
	Please do not keep traffic log enabled for too long, it will consume HDD space!

	To monitor a 3G modem or other kinds of devices that do not appear
	in the initial connections list, please connect the device first, then (re)start
	this application or choose a different connection and then use 'Switch interfaces'
	from application's tray menu.
	)
return
;================================================================
readLog:			; Read session log
;================================================================
Run, %logPath%\%logfile%,, UseErrorLevel
if ErrorLevel
	MsgBox, 0x2040, %appname%,
		(LTrim
		There is no log file yet or it has been erased.
		Please complete at least one session (connect-disconnect)
		before trying to view the log again.
		)
return
;================================================================
readTLog:		; Read traffic logs
;================================================================
FileSelectFile, i, 3, %logPath%\%sessTfile%, Select log file to read:, Log files (*.txt)
if (!i OR ErrorLevel)
	return
Run, %i%,, UseErrorLevel
return
;================================================================
pause:
;================================================================
isPaused := !isPaused
Menu, Tray, ToggleCheck, Pause
SetTimer, NetMeter, % isPaused ? "Off" : "On"
if isPaused
	{
	overlay2 .= ",15"
	overlay3 .= ",15"
	}
else
	{
	StringTrimRight, overlay2, overlay2, 3
	StringTrimRight, overlay3, overlay3, 3
	}
Sleep, %PollMs%
SetTrayIcon(hMain, 1, "i" hIL1 "|1" (isPaused ? "|3" : ""))
RepaintWindow(hPic2)
RepaintWindow(hPic3)
return
;================================================================
logSess:
;================================================================
sess := A_TickCount-sess
sessD := dnNew-lastDn, sessU := upNew-lastUp, sessF := GetDurationFormat2(sess, "s")
if !(sessD + sessU)
	return
reason := discon ? "Disconnected" : A_ExitReason
FileAppend, On: %CntOn% Off: %CntOff%`tSession: %sessF%`tDown: %sessD%`tUp: %sessU%`t%reason%`n
	, %logPath%\%logfile%
return
;================================================================
NetMeter:
;================================================================
/*
IF_OPER_STATUS_NON_OPERATIONAL=0	; LAN disabled
IF_OPER_STATUS_UNREACHABLE=1		; WAN not connected
IF_OPER_STATUS_DISCONNECTED=2		; LAN cable disconnected / WAN no carrier
IF_OPER_STATUS_CONNECTING=3		; WAN connecting
IF_OPER_STATUS_CONNECTED=4		; WAN connected
IF_OPER_STATUS_OPERATIONAL=5		; LAN connected
*/
SetTimer, NetMeter, Off
if discon
	if DllCall("GetOverlappedResult", Ptr, hEvent, Ptr, &ovl, PtrP, sz, "UInt", 0)
		{
		tb := ifrow := 0
		if (rr := GetIfTable(tb, 1))
			{
			if !ochIdx
				ochIdx := chIdx
			Loop, %adCnt%	; find the desired interface/adapter and use a pointer to its struct
				{
				i := NumGet(tb, 4+860*(A_Index-1)+512, "UInt")	; Index (infamous IF_INDEX)
				if ((i & 0xFFFF)=choice)
					{
					chIdx := i
					break
					}
				}

			if !(r := GetIfEntry(ifrow, chIdx))
				{
				FormatTime, CntOn,, yyyy.MM.dd HH:mm:ss
				discon := 0, sess := A_TickCount
				WSACloseEvent(hEvent), VarSetCapacity(ovl, 0)
				FormatTime, t,, dd.MM.yyyy HH:mm:ss
				OutputDebug, Reconnected on %t%.
				overlay2 := Ics%iNo% "," (19+atype)
				overlay3 := Ics%iNo% "," (isShared ? "16," : "") (isDefault ? "17," : "") (19+atype)
				}
			else
				{
				OutputDebug,
				(LTrim
				Event signaled %hEvent%,
				GetIfTable() returned %rr%,
				GetIfEntry() failed,
				Old interface index was %ochIdx%,
				New interface index is %chIdx%,
				Choice is %choice%.
				)
				SetTimer, NetMeter, -5000
				return
				}
			}
		}
	else
		{
		SetTimer, NetMeter, -5000
		return
		}
statVis := GetWinVisible(hStat)								; Get visibility for Status window
statMain := GetWinVisible(hMain)								; Get visibility for Main window
ics := InternetGetConnectedState(st, 0)	; TRUE is OK, parse 'st' to check out a particular connection
if !(r := GetIfEntry(ifrow, chIdx))
	{
	stAdm := NumGet(ifrow, 540)			; doesn't work !!!
	stOp := NumGet(ifrow, 544)			; doesn't work !!!
	}
;tooltip, stOp=%stOp%
if (!discon && (r OR  InStr(badStat, stOp)))	; if stOp in 0,2,3
	{
	r := FormatMessage("Line " A_LineNumber-4 ": GetIfEntry()", r)
	FormatTime, CntOff,, yyyy.MM.dd HH:mm:ss
	SetFormat, Integer, H
	st+=0
	hEvent := WSACreateEvent()	; Do it here to get a hex number for the handle
	SetFormat, Integer, D
	stOp++
	stat := stDesc%stOp%
	iNo := stOp=3 ? 5 : 6
	overlay2 := Ics%iNo% "," (13+iNo)
	overlay3 := Ics%iNo% "," (isShared ? "16," : "") (isDefault ? "17," : "") (13+iNo)
	SetTrayIcon(hMain, 1, "i" hIL1 "|1|" (iNo>5 ? 2 : 1))
	if statMain=1
		RepaintWindow(hPic2)
	if statVis=1
		{
		GuiControl, 2:, dbg, % FormatMessage("GetIfEntry()", r)
		RepaintWindow(hPic3)
		}
	discon=1							; Flag a disconnected interface
	if logSess
		gosub logSess
	if !NotifyAddrChange(hEvent, ovl)		; Only works on Win2000+ :-(
		{
		WSACloseEvent(hEvent)
		VarSetCapacity(ovl, 0)
		hEvent := 0
		}
	OutputDebug,
		(LTrim
		%r%
		InternetGetConnectedState() returned %ics%,
		Capabilities are %st%,
		Interface index is %chIdx%,
		Last status was %stat%.
		Event created: %hEvent%
		Timer is lax.`n
		)
	SetTimer, NetMeter, -5000
	return
	}
dspeed := SizeUnits(NumGet(ifrow, 524), f:="As" ut, "3.1") " " f
cRead := A_TickCount, Poll := (cRead - oRead)/1000, oRead := cRead
dnNew := NumGet(ifrow, 552), upNew  := NumGet(ifrow, 576)		; New readings for upload/download
dnRate := Round(dnNew - dnOld), upRate := Round(upNew - upOld)	; Current up/down rates [bytes]
;dnVal := dnRate/(PollMs/1000), upVal := upRate/(PollMs/1000)		; Current up/down rates [bps]
dnVal := dnRate/Poll, upVal := upRate/Poll						; Current up/down rates [bps]

csD := SizeUnits(dnVal, f :="A" ut, "3.1") f "ps"					; Current download speed
csU := SizeUnits(upVal, f :="A" ut, "3.1") f "ps"					; Current upload speed
fU := GetNumberFormat(upNew, 0, "-.3,0")						; Grouping-formatted new readings
fD := GetNumberFormat(dnNew, 0, "-.3,0")						; Grouping-formatted new readings
ufD := SizeUnits(dnNew, f:="A" ut) f						; Short formatted readings for 9x tray tip
ufU := SizeUnits(upNew, f:="A" ut) f						; Short formatted readings for 9x tray tip
tot := SizeUnits((upNew+dnNew), f:="A" ut, "3.1") " " f		; Total up+down traffic in current session
ccs := NumGet(ifrow, 544)+1, stat := stDesc%ccs%				; Current connection status

; Disconnected=5, None=4, Recv=3, Send=2, Recv/Sent=1
iNo := (ccs < stMin+1) ? 5 : dnRate=0 ? (upRate=0 ? 4 : 2) : (upRate=0 ? 3 : 1)
overlay2 := Ics%iNo% "," (19+atype)
overlay3 := Ics%iNo% "," (isShared ? "16," : "") (isDefault ? "17," : "") (19+atype)

; averages
if dnRate
;	avgD := (avgD + dnRate/PollMs)/2
	avgD := (avgD + dnRate/Poll)/2
if upRate
;	avgU := (avgU + upRate/PollMs)/2
	avgU := (avgU + upRate/Poll)/2
vavgD := SizeUnits(avgD, f:="A" ut, "3.1") f
vavgU := SizeUnits(avgU, f:="A" ut, "3.1") f
dnMax := dnVal > dnMax ? dnVal : dnMax
upMax := upVal > upMax ? upVal : upMax
vdnMax := SizeUnits(dnMax, f:="A" ut, "3.1") f
vupMax := SizeUnits(upMax, f:="A" ut, "3.1") f
if statVis=1
	{
	; packets
	d0 := 0, u0 := 0
	Loop, 5
		dp%A_Index% := NumGet(ifrow, 552+4*A_Index)
	d0 := GetNumberFormat(NumGet(ifrow, 556), 0, "-.3,0")
	Loop, 4
		u0 += (up%A_Index% := NumGet(ifrow, 576+4*A_Index))
	u0 := GetNumberFormat(u0, 0, "-.3,0")
/*
	interface #%chIdx%
	upload packets: %u0%`t(%up1%+%up2%+%up3%+%up4%)
	dnload packets: %d0%`t(%dp1% %dp2% %dp3% %dp4% %dp5%)
*/

	if (iNo != prv_iNo)
		RepaintWindow(hPic3)
	if (stat != prv_stat)
		GuiControl, 2:, stDesc, %stat%
	if (dspeed != prv_sp)
		GuiControl, 2:, dspeed, %dspeed%ps
	if GetBestRoute(buf, "4.2.2.2", ip1)	; try with zero? no, returns weird time! (Internet connection)
		GetBestRoute(buf, "127.0.0.1", ip1)	; Local Area Network connection
	GuiControl, 2:, dur, % GetDurationFormat2(NumGet(buf, 28)*1000, "s", 1)
	vxx1 := atype=1 ? u0 : fU, vxx2 := atype=1 ? d0 : fD
	vxx3 := atype=1 ? fU : "0%", vxx4 := atype=1 ? fD : "0%"
	IfNotEqual, vxx1, %pvxx1%, GuiControl, 2:, u0, %vxx1%
	IfNotEqual, vxx2, %pvxx2%, GuiControl, 2:, d0, %vxx2%
	IfNotEqual, vxx3, %pvxx3%, GuiControl, 2:, tctl2, %vxx3%	; to be fixed (compression)
	IfNotEqual, vxx4, %pvxx4%, GuiControl, 2:, tctl3, %vxx4%	; to be fixed (compression)
;	GuiControl, 2:, dbg, Admin status: %stAdm%
	pvxx1 := vxx1, pvxx2 := vxx2, pvxx3 := vxx3, pvxx4 := vxx4
	}
tray := w9x ? "S: " dspeed "ps Up: " ufU " Dn: " ufD " Tot: " tot
	: "Speed: " dspeed "ps`nSent: " fU " bytes`nRecv: " fD " bytes`nTotal: " tot
SetTrayIcon(hMain, 1, "t" tray)								; Update tray tooltip
if (iNo != prv_iNo)
	SetTrayIcon(hMain, 1, "i" hIL1 "|" Ics%iNo%)				; Update tray icon

if statMain=1												; Update the GUI Data
	{
	GuiControl,, mws0, % dnVal ? csD : ""					 	; Current download speed
	GuiControl,, mws2, % upVal ? csU : ""						; Current upload speed
	GuiControl,, mws3, %fD% bytes							; Total download for current session
	GuiControl,, mws4, %fU% bytes							; Total upload for current session
	GuiControl,, mws6, % dnMax ? vdnMax "ps" : ""				; Max. download speed
	GuiControl,, mws7, % upMax ? vupMax "ps" : ""				; Max. upload speed
	GuiControl,, mws9, % avgD ? vavgD "ps" : ""				; Average download speed
	GuiControl,, mws10, % avgU ? vavgU "ps" : ""				; Average upload speed
	if (iNo != prv_iNo)										; Update main window icon
		RepaintWindow(hPic2)
	}
prv_iNo := iNo, dnOld := dnNew, upOld := upNew, prv_stat := stat, prv_sp := dspeed
if logTraf
	SetTimer, getTraf, -1
PollMs := (dnRate + upRate = 0) ? 1000 : 1000*mRate
SetTimer, NetMeter, -%PollMs%
Return
;================================================================
getTraf:
;================================================================
clist=
FormatTime, conchk,, yyyy.MM.dd HH:mm:ss
if GetTcpTable(Tbuf, 1)
	if (TCnum := NumGet(Tbuf, 0, "UInt"))	; Total connections number (TCP)
		Loop, %TCnum%
	{
	t := NumGet(Tbuf, 4+20*(A_Index-1), "UInt"), st := cState%t%		; dwState
	la := Net_MakeIP(NumGet(Tbuf, 4+20*(A_Index-1)+4, "UInt"))			; dwLocalAddr
	lp := ntohs(NumGet(Tbuf, 4+20*(A_Index-1)+8, "UShort"))			; dwLocalPort (low word)
	ra := Net_MakeIP(n := NumGet(Tbuf, 4+20*(A_Index-1)+12, "UInt"))	; dwRemoteAddr
	rp := ntohs(NumGet(Tbuf, 4+20*(A_Index-1)+(n ? 16 : 18), "UShort"))	; dwRemotePort (high word)
	conn := "TCP`t" la ":" lp "`t" ra ":" rp "`t" st "`n"
	if !logLTraf
		if (n=0 OR IsLocalAddress(ra))
			continue
	if !InStr(lastTlist, conn)
		{
	lan := gethostbyaddr(la, info) ? MulDiv(NumGet(info, 0, Ptr), "AStr") : la	; Resolve local address
	ran := gethostbyaddr(ra, info) ? MulDiv(NumGet(info, 0, Ptr), "AStr") : ra	; Resolve remote address
	TClist .= conchk "`tTCP`t" lan ":" lp "`t" ran ":" rp "`t" st "`n"
;	OutputDebug, %conn%
		}
	clist .= conn
	}
if GetUdpTable(Tbuf, 1)
	if (TCnum := NumGet(Tbuf, 0, "UInt"))	; Total connections number (TCP)
		Loop, %TCnum%
		{
		la := Net_MakeIP(NumGet(Tbuf, 4+8*(A_Index-1), "UInt"))		; dwLocalAddr
		lp := ntohs(NumGet(Tbuf, 4+8*(A_Index-1)+4, "UShort"))			; dwLocalPort (low word)
		conn := "UDP`t" la ":" lp "`t*.*`n"
		if !InStr(lastTlist, conn)
			{
	lan := gethostbyaddr(la, info) ? MulDiv(NumGet(info, 0, Ptr), "AStr") : la	; Resolve local address
			TClist .= conchk "`tUDP`t" lan ":" lp "`t*.*`n"
;			OutputDebug, %conn%
			}
		clist .= conn
		}
lastTlist := clist
;================================================================
flushTraf:
if (StrLen(TClist) >= maxTSlen OR (isLogTraf && !logTraf) OR exit)
	{
	FileGetSize, szSess, %logPath%\%sessTfile%
	if (szSess + StrLen(TClist) > maxLLsize*1048576)
		{
		szA := maxLLsize*1048576 - szSess
		StringLeft, ts1, TClist, %szA%
		szM := InStr(ts1, "`n", 0, 0)
		StringLeft, ts1, TClist, % szM-1
		StringTrimLeft, TClist, TClist, %szM%
		if ts1
			FileAppend, %ts1%, %logPath%\%sessTfile%
		SplitPath, sessTfile,,, sessE, sessF
		if c := InStr(sessF, "[")
			StringLeft, sessF, sessF, c-2
		Loop
			if !FileExist(logPath "\" sessF " [" A_Index "]." sessE)
				{
				sessTfile := sessF " [" A_Index "]." sessE
				break
				}
		}
		FileAppend, %TClist%, %logPath%\%sessTfile%
		TClist := ""
	loggedT=1
		}
else loggedT=
return
;================================================================
ShowHide:
;================================================================
RepaintWindow(hPic2)
v := GetWinVisible(hMain)
if v=0
	{
	Gui, Show, AutoSize NA
	wincol()
	}
else if v=1
	Gui, Hide
else if v=2
	{
	Gui, Show, Restore NA
	wincol()
	}
return
;================================================================
pin:
;================================================================
TME_Expanded := pin
pin := !pin
GuiControl,, bPin, % pin ? Chr(c7) : Chr(c6)
if pin
	{
	Loop, 10
		GuiControl, Show, mws%A_Index%
	Gui, Show, AutoSize NA
	}
else tID := TrackMouseEvent(hMain, "L")
return
;================================================================
GuiContextMenu:
;================================================================
Menu, Tray, Show
if !pin
	wincol()
Return
;================================================================
disable:	; Currently set to DHCP Release, to be modified !
;================================================================
if DHCPEnDis(cName, 0)
	{
	GuiControl, 2:, tctl7, &Enable
	GuiControl, 2:+genable, tctl7
	}
return
;================================================================
enable:	; Currently set to DHCP Release, to be modified !
;================================================================
if DHCPEnDis(cName, 1)
	{
	GuiControl, 2:, tctl7, &Disable
	GuiControl, 2:+gdisable, tctl7
	}
return
;================================================================
settings:
#include %A_ScriptDir%\lib
#include *i Settings.ahk
;================================================================
DHCPEnDis(aName, a=0)	; DHCP release/renew (default=release)
;================================================================
{
if !GetInterfaceInfo(buf)		; returns IP_ADAPTER_INDEX_MAP structs
	{
	r := A_ThisFunc "`n" ErrorLevel
	OutputDebug, %r%
	MsgBox, 0x42010, %appname% error, %r%
	return
	}
if !(num := NumGet(buf, 0, "UInt"))
	{
	r := A_ThisFunc "`nNo network adapters found.`nPlease enable them in Device Manager."
	MsgBox, 0x42010, %appname% error, %r%
	return
	}
Loop, %num%			; find the desired interface/adapter and use a pointer to its struct
	if (MulDiv(&buf+8+260*(A_Index-1), "Str")=aName)	; Name (Not WCHAR in Win9x !)
		{
		p := &buf+4+260*(A_Index-1)	; pointer to IP_ADAPTER_INDEX_MAP
		break
		}
if !p
	return
if !(r := a ? IpRenewAddress(p) : IpReleaseAddress(p))
	{
	OutputDebug, % r := ErrorLevel
	MsgBox, 0x42010, %appname%, %A_ThisFunc%`n%r%
	}
else return r
}
;================================================================
help(wP, lP, msg, hwnd)
;================================================================
{
Global ishelpon, isdeton, ctxhlp, hMain, hStat, hDet
iCtrlId := NumGet(lP+0, 8, "UInt")
err := "<no help available for control " iCtrlId " >"
sect := (hwnd=hStat) ? "Status" : (hwnd=hDet) ? "Details" : "Other"
IniRead, h, %ctxhlp%, %sect%, %iCtrlId%, %err%
if InStr(h, "@")=1
	{
	StringSplit, s, h, @
	s2 := s2 ? s2 : sect
	IniRead, h, %ctxhlp%, %s2%, %s3%, %err%
	}
h := h ? h : err
StringReplace, h, h, |, `n, All
isdeton := FALSE
Tooltip, %h%
ishelpon := TRUE
return 1
}
;================================================================
winmove(wP, lP, msg, hwnd)		; WM_LBUTTONDOWN
;================================================================
{
Global
SetFormat, Integer, H
hwnd+=0
SetFormat, Integer, D
if ishelpon
	{
	Tooltip
	ishelpon=
	}
if GetWinVisible(hSet)=1
	if hwnd in %hSetIconset%
		{
		MouseGetPos,,,, detLbl, 0
		StringTrimLeft, i, detLbl, 6
		GuiControl, 99:MoveDraw, setS, % "x" 51+38*(i-6)
		setIS := i-5
		Gui, 99:Submit, NoHide
		GuiControl, 99:Enable, B99app
		}
if (hwnd != hMain && hwnd != hPic2)
	return
;SetTimer, resurrect, -5
DllCall("SetCursor", Ptr, hCursM)
PostMessage, 0xA1, 2,,, ahk_id %hMain%		; WM_NCLBUTTONDOWN
}
resurrect:
tID := !TrackMouseEvent(hMain, "C")
return
;================================================================
winexp(wP, lP, msg, hwnd)		; WM_MOUSEMOVE
;================================================================
{
Global
Static i
;Critical
SetFormat, Integer, H
hwnd+=0
SetFormat, Integer, D
if (hwnd=hDet)
	{
	MouseGetPos,,,, detLbl, 0
	if (detLbl != "Static1" OR isdeton OR ishelpon)
		return 0
	Tooltip, %desc%
	isdeton := 1
	TrackMouseEvent(hwnd, "C")
;Sleep, 0	; unsure !
	TrackMouseEvent(hwnd)
	}
if (hwnd = hMain && !TME_Expanded && !pin && !noAE && !tID)
	{
	Loop, 9
		GuiControl, Show, % "mws" A_Index+1
	Gui, Show, AutoSize NA
	TrackMouseEvent(hwnd, "C")
;Sleep, 0	; unsure !
	tID := TrackMouseEvent(hwnd)
	TME_Expanded=1
	}
if GetWinVisible(hSet)=1
	{
	if hwnd in %hSetIconset%
		{
		MouseGetPos,,,, detLbl, 0
		StringTrimLeft, i, detLbl, 6
		if (i != lastSetIcon)
			GuiControl, 99:MoveDraw, setC, % "x" 51+38*(i-6)
		GuiControl, 99:Show, setC
		DllCall("SetCursor", Ptr, hCursH)
		lastSetIcon := i
		}
	else
		GuiControl, 99:Hide, setC
	}
}
;================================================================
wincol()				; WM_MOUSELEAVE
;================================================================
{
Global
;Critical
MouseGetPos,,,, hCCtrl, 2
if TME_Expanded
	{
	if (hCCtrl != hPic2 && hCCtrl != hPin)
		{
		Loop, 9
			GuiControl, Hide, % "mws" A_Index+1
		Gui, Show, AutoSize NA
		TME_Expanded := 0
		tID := 0
		}
	else
		{
		TrackMouseEvent(hMain, "C")
		Sleep, 0
		tID := TrackMouseEvent(hMain)
		}
	}
if isdeton
	{
	Tooltip
	isdeton=
	}
return 0
}
;================================================================
notrk(wP, lP, msg, hwnd)		; WM_NCLBUTTONDOWN
;================================================================
{
Global
if ishelpon
	{
	Tooltip
	ishelpon=
	}
if isdeton
	{
	Tooltip
	isdeton=
	}
if (hwnd != hMain && !pin && noAE)
	{
	r := TrackMouseEvent(hMain, "C", 800)
	TME_Expanded := tID := 0
	}
else if (hwnd = hMain && TME_Expanded && !pin && !noAE && !tID)
	tID := TrackMouseEvent(hwnd)
else TME_Expanded := tID := 0
;outputdebug, hwnd=%hwnd% hMain=%hMain% TME_Expanded=%TME_Expanded% pin=%pin% noAE=%noAE% tID=%tID% r=%r%
}
;================================================================
onicon(wP, lP, msg, hwnd)		; WM_NCMOUSEHOVER, WM_NCMOUSELEAVE
{
tooltip, msg=%msg%
}
;================================================================
MakeIP(n)
;================================================================
{
Global
return MulDiv(inet_ntoa(n), "AStr")
}
;================================================================
Item(strg, pos)
;================================================================
{
Loop, Parse, strg, CSV
	if (pos=A_Index)
		return A_LoopField
}
;================================================================
;================================================================
;	WINDOWPROC OVERRIDE
;================================================================
WindowProc(hwnd, msg, wP, lP)
{
Static
Global Ptr, PtrP, AStr, AW, w9x, hPic1, hPic2, hPic3, hIL1, hIL2, hIL3, WPOld1, WPOld2, WPOld3, overlay1, overlay2, overlay3
Critical
if !init
	{
	PtrSize := A_PtrSize ? A_PtrSize : 4
	SetFormat, Integer, H
	hGdi := DllCall("GetModuleHandle", "Str", "gdi32.dll", Ptr)
	hUsr := DllCall("GetModuleHandle", "Str", "user32.dll", Ptr)
	hCctl := DllCall("GetModuleHandle", "Str", "comctl32.dll", Ptr)
	hpCCD := DllCall("GetProcAddress", Ptr, hGdi, AStr, "CreateCompatibleDC", Ptr)
	hpCCB := DllCall("GetProcAddress", Ptr, hGdi, AStr, "CreateCompatibleBitmap", Ptr)
	hpSO := DllCall("GetProcAddress", Ptr, hGdi, AStr, "SelectObject", Ptr)
	hpSSBM := DllCall("GetProcAddress", Ptr, hGdi, AStr, "SetStretchBltMode", Ptr)
	hpSBOE := DllCall("GetProcAddress", Ptr, hGdi, AStr, "SetBrushOrgEx", Ptr)
	hpBB := DllCall("GetProcAddress", Ptr, hGdi, AStr, "BitBlt", Ptr)
	hpSB := DllCall("GetProcAddress", Ptr, hGdi, AStr, "StretchBlt", Ptr)
	hpDO := DllCall("GetProcAddress", Ptr, hGdi, AStr, "DeleteObject", Ptr)
	hpDD := DllCall("GetProcAddress", Ptr, hGdi, AStr, "DeleteDC", Ptr)
	hpIR := DllCall("GetProcAddress", Ptr, hUsr, AStr, "InvalidateRect", Ptr)
	hpBP := DllCall("GetProcAddress", Ptr, hUsr, AStr, "BeginPaint", Ptr)
	hpEP := DllCall("GetProcAddress", Ptr, hUsr, AStr, "EndPaint", Ptr)
;	hpCWP := DllCall("GetProcAddress", Ptr, hUsr, AStr, "DefWindowProc" AW, Ptr)
	hpCWP := DllCall("GetProcAddress", Ptr, hUsr, AStr, "CallWindowProc" AW, Ptr)
	hpILGII := DllCall("GetProcAddress", Ptr, hCctl, AStr, "ImageList_GetImageInfo", Ptr)
	hpILD := DllCall("GetProcAddress", Ptr, hCctl, AStr, "ImageList_Draw", Ptr)
	SetFormat, Integer, D
	WinGetPos,,, w1, h1, ahk_id %hPic1%
	WinGetPos,,, w2, h2, ahk_id %hPic2%
	WinGetPos,,, w3, h3, ahk_id %hPic3%
	init=1
	}
if (hwnd=hPic1)
	{
	w := w1
	h := h1
	WPOld := WPOld1
	ovl := overlay1
	hIL := hIL2
	}
else if (hwnd=hPic2)
	{
/*
;tooltip, msg=%msg%
	if (msg=0x201)
		{
		tooltip, L click
;		return winmove(wP, lP, msg, hwnd)
		}
	if (msg=0x200 OR wP=0x200 OR lP=0x200)
		{
		tooltip, Mouse move`nmsg=%msg% wP=%wP% lP=%lP%
;		return winexp(wP, lP, msg, hwnd)
		}
	if (msg=0x2A0 or msg=0x2A2 or msg=0xA0 or msg=0x2A1 or msg=0x2A3)
		tooltip, msg=%msg%
*/
	w := w2
	h := h2
	WPOld := WPOld2
	ovl := overlay2
	hIL := hIL2
	}
else if (hwnd=hPic3)
	{
	w := w3
	h := h3
	WPOld := WPOld3
	ovl := overlay3
	hIL := hIL3
	}
if (msg=0xF)	; WM_PAINT
	{
	VarSetCapacity(IMAGEINFO, 24+2*PtrSize, 0)
	DllCall(hpILGII, Ptr, hIL, "Int", 0, Ptr, &IMAGEINFO)	; ImageList_GetImageInfo
	bx := NumGet(IMAGEINFO, 8+2*PtrSize, "Int")
	by := NumGet(IMAGEINFO, 12+2*PtrSize, "Int")
	bw := NumGet(IMAGEINFO, 16+2*PtrSize, "Int")-bx
	bh := NumGet(IMAGEINFO, 20+2*PtrSize, "Int")-by
DllCall("ShowWindow", Ptr, hwnd, "UInt", 0)
DllCall("ShowWindow", Ptr, hwnd, "UInt", 8)
	VarSetCapacity(ps, 64, 0)
	hDC := DllCall(hpBP, Ptr, hwnd, Ptr, &ps)			; BeginPaint
	DllCall(hpSSBM, Ptr, hDC, "UInt", w9x ? 3 : 4)		; SetStretchBltMode COLORONCOLOR / STRETCH_HALFTONE
	DllCall(hpSBOE, Ptr, hDC, "Int", 0, "Int", 0, Ptr, 0)	; SetBrushOrgEx
	DllCall("SetBkMode", Ptr, hDC, "UInt", 2)
/*
	hDC2 := DllCall(hpCCD, Ptr, hDC)					; CreateCompatibleDC
	hBmp1 := DllCall(hpCCB , Ptr, hDC, "Int", 1, "Int", 1)	; CreateCompatibleBitmap
	hBo := DllCall(hpSO, Ptr, hDC2, Ptr, hBmp1)			; SelectObject
	DllCall(hpBB									; BitBlt
		, Ptr, hDC2
		, "Int", 0
		, "Int", 0
		, "Int", 1
		, "Int", 1
		, Ptr, hDC
		, "Int", 0
		, "Int", 0
		, "UInt", 0x00CC0020)	; SRC_COPY
	DllCall(hpSB									; StretchBlt
		, Ptr, hDC
		, "Int", 0
		, "Int", 0
		, "Int", W
		, "Int", H
		, Ptr, hDC2
		, "Int", 0
		, "Int", 0
		, "Int", 1
		, "Int", 1
		, "UInt", 0x00CC0020)	; SRC_COPY
	DllCall(hpSO, Ptr, hDC2, Ptr, hBo)					; SelectObject
	DllCall(hpDO, Ptr, hBmp1)						; DeleteObject
	DllCall(hpDD, Ptr, hDC2)							; DeleteDC
*/
	Loop, Parse, ovl, CSV
		DllCall(hpILD								; ImageList_Draw
			, Ptr, hIL
			, "UInt", A_LoopField-1
			, Ptr, hDC
			, "Int", 0		; x
			, "Int", 0		; y
			, "UInt", 1)	; fStyle (ILD_NORMAL/TRANSPARENT)
	
	hDC2 := DllCall(hpCCD, Ptr, hDC)					; CreateCompatibleDC
	hBmp := DllCall(hpCCB, Ptr, hDC, "Int", W, "Int", H)	; CreateCompatibleBitmap
	hBo := DllCall(hpSO, Ptr, hDC2, Ptr, hBmp)			; SelectObject
	DllCall(hpSSBM, Ptr, hDC2, "UInt", w9x ? 3 : 4)		; SetStretchBltMode COLORONCOLOR/STRETCH_HALFTONE
	DllCall(hpSBOE, Ptr, hDC2, "Int", 0, "Int", 0, Ptr, 0)	; SetBrushOrgEx Required in 9x with RP9/ME updates
	DllCall(hpSB									; StretchBlt
		, Ptr, hDC2
		, "Int", 0
		, "Int", 0
		, "Int", W
		, "Int", H
		, Ptr, hDC
		, "Int", 0
		, "Int", 0
		, "Int", bw
		, "Int", bh
		, "UInt", 0x00CC0020)						; SRC_COPY
	DllCall(hpSO, Ptr, hDC2, Ptr, hBo)					; SelectObject
	DllCall(hpDO, Ptr, hBmp)							; DeleteObject
	DllCall(hpDD, Ptr, hDC2)							; DeleteDC
	DllCall(hpEP, Ptr, hwnd, Ptr, &ps)					; EndPaint
	return 0
	}
return DllCall(hpCWP, Ptr, WPOld, Ptr, hwnd, "UInt", msg, Ptr, wP, Ptr, lP)
}
;================================================================
SetTrayIcon(hwnd, i, p="")
;================================================================
{
Static
Global Ptr, AW, A_CharSize, w9x
PtrSize := A_PtrSize ? A_PtrSize : 4
flg := msg := hIcon := tt := v1 := v2 := v3 := 0
if !p
	act%i%=2
else Loop, Parse, p, CSV, %A_Space%
	{
	StringLeft, d, A_LoopField, 1
	StringTrimLeft, v, A_LoopField, 1
	StringSplit, v, v, |
	if d=m				; Set message
		{
		msg := v
		flg += 1			; NIF_MESSAGE
		}
	if d=i				; Set icon
		{
		if v1 is integer
			hIcon := DllCall("ImageList_GetIcon", Ptr, v1, "UInt", v2-1, "UInt", 1+(v3<<8))	; ILD_TRANSPARENT
		else if FileExist(v1)
			{
			if (lib := DllCall("LoadLibrary", "Str", v1))
				{
				hIcon := DllCall("LoadImage" AW
							, Ptr, lib
							, "Int", v2
							, "UInt", 1
							, "Int", 16
							, "Int", 16
							, "UInt", 0x20
							, Ptr)
				DllCall("FreeLibrary", Ptr, lib)
				}
			else hIcon := DllCall("LoadImage" AW
							, Ptr, 0
							, "Str", v
							, "UInt", 1
							, "Int", 16
							, "Int", 16
							, "UInt", 0x10
							, Ptr)
			}
		if hIcon
			flg += 2		; NIF_ICON
		}
	if d=t				; Set tooltip
		{
		tt := v
		flg += 4			; NIF_TIP
		}
	if d=v				; Set version
		{
		ver := v
		act%i%=4		; NIM_SETVERSION
		}
	}
cbSize := w9x ? 88 : ver="3" ? 32+2*PtrSize+448*A_CharSize : 48+3*PtrSize+448*A_CharSize	; some say 88/488/504
VarSetCapacity(NID, cbSize, 0)				; NOTIFYICONDATA
NumPut(cbSize, NID, 0, "UInt")				; cbSize
NumPut(hwnd, NID, 4, Ptr)					; hWnd
NumPut(i, NID, 4+PtrSize, "UInt")				; uID
NumPut(flg, NID, 8+PtrSize, "UInt")			; NIF_ICON | NIF_MESSAGE
NumPut(msg, NID, 12+PtrSize, "UInt")			; uCallbackMessage
NumPut(hIcon, NID, 16+PtrSize, Ptr)			; hIcon
if tt
	DllCall("shlwapi\StrCpyNW", Ptr, &NID+16+2*PtrSize, "Str", tt, "UInt", (w9x ? 63 : 127*A_CharSize))	; szTip
if act%i%=4
	NumPut(ver, NID, 24+2*PtrSize+384*A_CharSize, "UInt")		; uVersion
DllCall("shell32\Shell_NotifyIcon" AW, "UInt", act%i%, Ptr, &NID)	; NIM_ ADD=0/MODIFY=1/DELETE=2
if hIcon
	DllCall("DestroyIcon", Ptr, hIcon)
if !act%i%
	act%i%=1
else if act%i% in 2,4
	act%i%=0
}
;================================================================
traymsg(wP, lP, msg, hwnd)
;================================================================
{
if (lP=0x205)	; WM_RBUTTONUP
	Menu, Tray, Show
else if (lP=0x203)	; WM_LBUTTONDBLCLK
	{
	SetTimer, showStat, off
	}
else if (lP=0x202)	; WM_LBUTTONUP
	SetTimer, showStat, -250
}
;================================================================
RepaintWindow(hwnd)
;================================================================
{
Global
;r := DllCall("InvalidateRect", Ptr, hwnd, Ptr, 0, "UInt", 1)
;r += DllCall("UpdateWindow", Ptr, hwnd)
;WinHide, ahk_id %hwnd%
;WinShow, ahk_id %hwnd%
WinSet, Redraw,, ahk_id %hwnd%
;return r
}
;================================================================
retray(wP, lP, msg, hwnd)
{
Global
SetTrayIcon(hMain, 1)									; Delete tray icon
if !w9x
	SetTrayIcon(hMain, 1, "v3")							; NOTIFYICON_VERSION
SetTrayIcon(hMain, 1, "i" hIL1 "|" Ics%iNo% ",m0x1966,t" tray)	; Set updated tray icon
return DllCall("DefWindowProc" AW, Ptr, hwnd, "UInt", msg, Ptr, wP, Ptr, lP)
}
;================================================================
;#include %A_ScriptDir%\lib
#include func_DebugFunctions.ahk
#include func_FileMap.ahk
#include func_FormatMessage.ahk
#include func_FormatNumber 1.3.ahk
#include func_GetCSIDL.ahk
#include func_GetDurationFormat.ahk
#include func_GetFileVersionInfo.ahk
#include func_GetNumberFormat.ahk
#include func_GetWinVisible.ahk
#include func_ImageList 3.1.ahk
#include func_IpHlpApi.ahk
#include func_LV_FindItem.ahk
#include func_SizeUnits.ahk
#include func_TrackMouseEvent.ahk
#include func_wininet.ahk
#include func_ws2_32.ahk
#include Net.ahk
