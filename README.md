# NetMeterEx


Displays network activity on selected adapter in real time in a small floating window and/or in system tray. Details window mimics as close as possible the Windows ® XP dialog.

This script/application was initially intended for Windows® 9x systems which have no such tool by default. By now most such systems may not have much use for it except maybe for intranet. However it may be useful for recent Windows® versions which tend to completely hide network traffic from the user. If it still works on such new systems, that is.

The script has not been tested under Windows® 8 or later versions. It may or may not work. Use at your own risk. Don't come running to me if your computer blows up. :-)

If it works in some way or another please do not enable logging for a long time - or better not at all - because it may flood the drive and slow down the entire system.

Otherwise, enjoy!

Drugwash, 2023
